(function($){
	// 初始化div區塊
	$.fn.photoViewerInit = function(options) {  
    var settings = $.extend({
			css  : {
				float            : 'left', 
				border         	 : '1px solid red',
				color         	 : 'red',
				backgroundColor  : 'yellow', 
				width            : '100', 
				height           : '100', 
				margin           : '5', 
				textAlign        : 'center', 
				padding          : '5'	
			}
		}, options||{});
		
    return this.each(function() { 			
			$(this).css(settings.css);
    });
  };	
	// 在div區塊內顯示圖片
	$.fn.photoViewerShow = function(imageName, options) {  		
    return this.each(function() {
			// 在div元素內加入檔案名稱為imageName的img元素
			if (imageName != null) {
				var image = $('<img src="' + imageName + '" />');
				$(this).css({'width': options.width, 'height': options.height}).append(image);
			}
    });	  
	};
	// 在div區塊內隱藏圖片
	$.fn.photoViewerHide = function() {
		// 移除div元素的所有子元素
    return this.each(function() {
			$(this).empty();
    });	  
	};
})(jQuery);