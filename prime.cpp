#include <stdio.h>
int p[655350] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37 };
int c = 11;

int isPrime(int a) {
  for (int i=0; p[i]*p[i]<=a; i++) {
    if (a % p[i] == 0) return 0;
  }
  if (a > p[c]) { p[++c] = a; }
  return 1;
}
int main(int argc, char* argv[]) {
  int a, b, isPrime;

  printf ("請輸入兩個數字: ");
  scanf("%d %d", &a, &b);
  if (a <= 2) printf (" 2");
  for (int i=3; i<=b; i += 2) {
    isPrime = 1;
    for (int k=0; p[k]*p[k]<=i; k++) {
      if (i % p[k] == 0) {
        isPrime = 0;
        break;
      }
    }
    if (isPrime) {
      if (i > p[c]) { p[++c] = i; } // 如果不在清單中，則將此質數丟進清單
      if (i>=a) printf (" %d", i);
    }
  }
  printf ("\n");
  return 0;
}
