#include <stdio.h>
#include <string.h>

char ca[] = "abcdefghijklmnopqrstuvwxyz',.[]";
int d[31][31];

void dump() {
  printf ("    ");
  for (int i=0; i<31; i++) {
    printf ("   %c", ca[i]);
  }
  printf ("\n");
  for (int i=0; i<31; i++) {
    printf ("   %c", ca[i]);
    for (int j=0; j<31; j++) {
      printf ("%4d", d[i][j]);
    }
    printf ("\n");
  }
}
int idx(int c) {
  if (c>='a' && c<='z') return c-'a';
  if (c == '\'') return 26;
  if (c == ',') return 27;
  if (c == '.') return 28;
  if (c == '[') return 29;
  if (c == ']') return 30;
  return -1;
}
void ins(char* s) {
  int x, y;
  int n;
  for (n=0; n<strlen(s); n++) {
    if ((x = idx(s[n])) >= 0) break;
  }
  n++;
  for (int i=n; i<strlen(s); i++) {
    y = idx(s[i]);
    if (y >= 0) {
      d[x][y]++;
      x = y;
    }
  }
}
int main(int argc, char* argv[]) {
  char fn[] = "b.lime";
  FILE* fp;
  char buf[128];
  char a[10];

  bzero (d, 31*31*sizeof(int));
  fp = fopen("b.lime", "r");
  if (fp == NULL) {
    printf ("no lime database %s\n", fn);
    return 1;
  }
  while (fgets(buf, 128, fp) != NULL) {
    sscanf(buf, "%s ", a);
    ins(a);
  }
  fclose(fp);

  dump();
  return 0;
}
