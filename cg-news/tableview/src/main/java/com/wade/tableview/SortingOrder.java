package com.wade.tableview;

public enum SortingOrder {
    ASCENDING,
    DESCENDING
}
