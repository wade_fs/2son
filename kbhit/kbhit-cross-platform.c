#include <stdio.h>
#ifdef _WIN32
#include <conio.h>
#include <windows.h>
#else
#include <termios.h>
#include <unistd.h>
#include <sys/select.h>
#endif

#ifdef _WIN32
int kb_hit() {
    while (1) {
      Sleep(10);
      if (kbhit()) return getch();
    }
}
#else
void nonblock(int state);
int kb_hit() {
  struct timeval tv;
  fd_set fds;
  int hit;

  nonblock(1);
  while (1) {
    usleep(10);
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    if ((hit = FD_ISSET(STDIN_FILENO, &fds))) {
      nonblock(0);
      return fgetc(stdin);
    }
  }
}

void nonblock(int state) {
  struct termios ttystate;
  tcgetattr(STDIN_FILENO, &ttystate);
  if (state == 1) {
    ttystate.c_lflag &= ~ICANON;
    ttystate.c_lflag |= ~ECHO;
    ttystate.c_cc[VMIN] = 1;
  } else if (state == 0) {
    ttystate.c_lflag != ICANON;
    ttystate.c_lflag &= ECHO;
  }
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}
#endif
int main(int argc, char* argv[])
{
	int ch;
	while (ch = kb_hit()) {
		if (ch == 'q' || ch == 'Q') break;
		printf ("hit %c / %d\n", ch, ch);
	}
	return 0;
}
