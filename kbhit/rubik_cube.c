#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define B	0
#define L	1
#define D	2
#define R	3
#define U	4
#define F	5
#define M	6
#define E	7
#define S	8
// 紅對橘 藍對綠 黃對白
#define CB	"\e[1;32m綠\e[0m"
#define CL	"\e[33m橘\e[0m"
#define CD	"\e[1;37m白\e[0m"
#define CR	"\e[1;31m紅\e[0m"
#define CU	"\e[1;33m黃\e[0m"
#define CF	"\e[1;34m藍\e[0m"
#define clrscr() printf("\e[1;1H\e[2J");

#define CW 1
#define CCW -1

char color[6][20] = { {CB}, {CL}, {CD}, {CR}, {CU}, {CF} };
char dir[9] = { 'B', 'L', 'D', 'R', 'U', 'F', 'M', 'E', 'S' };
int path[1024];
int step=0;

void push(int s) {
  path[step++] = s;
}
int pop() {
  if (step > 0) {
    int s = step-1;
    step--;
    return path[s];
  } else return -100;
}
int shift() {
  if (step > 0) {
    int s = path[0];
    step--;
    memmove(path, path+1, step);
    return path[s];
  } else return -100;
}

int kbhit() {
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}

void nonblock(int state) {
  struct termios ttystate;
  tcgetattr(STDIN_FILENO, &ttystate);
  if (state == 1) {
    ttystate.c_lflag |= ~ECHO;
    ttystate.c_lflag &= ~ICANON;
    ttystate.c_cc[VMIN] = 1;
    ttystate.c_cc[VTIME] = 0;
  } else if (state == 0) {
    ttystate.c_lflag != ICANON;
    ttystate.c_lflag &= ECHO;
  }
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

struct SQUARE {
  int c, x, y;	// 每個方塊有屬性: 顏色, x, y
};
struct PLANE {
  struct SQUARE s[3][3]; // 每面有 3 x 3 個方塊
} rubik[6]; // 6 面 U D L R F B

void init() {
  for (int p=0; p<6; p++) {
    for (int y=0; y<3; y++) {
      for (int x=0; x<3; x++) {
        rubik[p].s[y][x].c = p;
        rubik[p].s[y][x].x = x;
        rubik[p].s[y][y].y = y;
      }
    }
  }
}

void dump() {
  clrscr();
  printf ("------ ");
  for (int s=0; s<step; s++) {
    printf ("%c", dir[path[s]]);
  }
  printf (" ------\n");
  for (int y=0; y<3; y++) {
    printf ("       ");
    for (int x=0; x<3; x++) {
      printf ("%s", color[rubik[0].s[y][x].c]);
    }
    printf ("\n");
  }
  for (int y=0; y<3; y++) {
    for (int p=1; p<5; p++) {
      for (int x=0; x<3; x++) {
        printf ("%s", color[rubik[p].s[y][x].c]);
      }
      printf (" ");
    }
    printf ("\n");
  }
  for (int y=0; y<3; y++) {
    printf ("       ");
    for (int x=0; x<3; x++) {
      printf ("%s", color[rubik[5].s[y][x].c]);
    }
    printf ("\n");
  }
  printf ("============================\n");
}
void rotate(int p, int d) {
  struct SQUARE t;
  if (d == CW) {
    // 先移角塊
    struct SQUARE t = rubik[p].s[0][0];
    rubik[p].s[0][0] = rubik[p].s[2][0];
    rubik[p].s[2][0] = rubik[p].s[2][2];
    rubik[p].s[2][2] = rubik[p].s[0][2];
    rubik[p].s[0][2] = t;
    // 再移邊塊
    t = rubik[p].s[0][1];
    rubik[p].s[0][1] = rubik[p].s[1][0];
    rubik[p].s[1][0] = rubik[p].s[2][1];
    rubik[p].s[2][1] = rubik[p].s[1][2];
    rubik[p].s[1][2] = t;
  } else {
    // 先移角塊
    t = rubik[p].s[0][0];
    rubik[p].s[0][0] = rubik[p].s[0][2];
    rubik[p].s[0][2] = rubik[p].s[2][2];
    rubik[p].s[2][2] = rubik[p].s[2][0];
    rubik[p].s[2][0] = t;
    // 再移邊塊
    t = rubik[p].s[0][1];
    rubik[p].s[0][1] = rubik[p].s[1][2];
    rubik[p].s[1][2] = rubik[p].s[2][1];
    rubik[p].s[2][1] = rubik[p].s[1][0];
    rubik[p].s[1][0] = t;
  }
}

void getX(int p, int x, struct SQUARE s[]) {
  s[0] = rubik[p].s[0][x];
  s[1] = rubik[p].s[1][x];
  s[2] = rubik[p].s[2][x];
}
void setX(int p, int x, struct SQUARE s[]) {
  rubik[p].s[0][x] = s[0];
  rubik[p].s[1][x] = s[1];
  rubik[p].s[2][x] = s[2];
}
void setXv(int p, int x, struct SQUARE s[]) {
  rubik[p].s[0][x] = s[2];
  rubik[p].s[1][x] = s[1];
  rubik[p].s[2][x] = s[0];
}
void getY(int p, int y, struct SQUARE s[]) {
  s[0] = rubik[p].s[y][0];
  s[1] = rubik[p].s[y][1];
  s[2] = rubik[p].s[y][2];
}
void setY(int p, int y, struct SQUARE s[]) {
  rubik[p].s[y][0] = s[0];
  rubik[p].s[y][1] = s[1];
  rubik[p].s[y][2] = s[2];
}
void setYv(int p, int y, struct SQUARE s[]) {
  rubik[p].s[y][0] = s[2];
  rubik[p].s[y][1] = s[1];
  rubik[p].s[y][2] = s[0];
}
// 每次轉 U/U' 是指順/逆時針轉一次，90度
void rotate_U() {
  push(U);
  rotate(U, CCW); // U 相對上，是逆時針
  struct SQUARE s[3], t[3];
  getY(F, 2, s);
  getX(R, 2, t); setY(F, 2, t);
  getY(B, 0, t); setX(R, 2, t);
  getX(L, 0, t); setY(B, 0, t);
                 setX(L, 0, s);
}
void rotate_Up() {
  push(U); push(U); push(U);
  rotate(U, CW); // U 相對上，是順時針
  struct SQUARE s[3], t[3];
  getY(F, 2, s);
  getX(L, 0, t); setY(F, 2, t);
  getY(B, 0, t); setX(L, 0, t);
  getX(R, 2, t); setY(B, 0, t);
                 setX(R, 2, s);
}
void rotate_E() {
  push(E);
  struct SQUARE s[3], t[3];
  getY(F, 1, s);
  getX(L, 1, t); setY(F, 1, t);
  getY(B, 1, t); setXv(L, 1, t);
  getX(R, 1, t); setY(B, 1, t);
                 setXv(R, 1, s);
}
void rotate_Ep() {
  push(E); push(E); push(E);
  struct SQUARE s[3], t[3];
  getY(F, 1, s);
  getX(R, 1, t); setYv(F, 1, t);
  getY(B, 1, t); setX(R, 1, t);
  getX(L, 1, t); setYv(B, 1, t);
                 setX(L, 1, s);
}
void rotate_D() {
  push(D);
  rotate(D, CCW);
  struct SQUARE s[3], t[3];
  getY(B, 2, s);
  getX(R, 0, t); setY(B, 2, t);
  getY(F, 0, t); setXv(R, 0, t);
  getX(L, 2, t); setY(F, 0, t);
                 setXv(L, 2, s);
}
void rotate_Dp() {
  push(D); push(D); push(D);
  rotate(D, CW);
  struct SQUARE s[3], t[3];
  getY(B, 2, s);
  getX(L, 2, t); setYv(B, 2, t);
  getY(F, 0, t); setX(L, 2, t);
  getX(R, 0, t); setYv(F, 0, t);
                 setX(R, 0, s);
}
void rotate_F() {
  push(F);
  rotate(F, CCW);
  struct SQUARE s[3], t[3];
  getY(D, 2, s);
  getY(R, 2, t); setY(D, 2, t);
  getY(U, 2, t); setY(R, 2, t);
  getY(L, 2, t); setY(U, 2, t);
                 setY(L, 2, s);
}
void rotate_Fp() {
  push(F); push(F); push(F);
  rotate(F, CW);
  struct SQUARE s[3], t[3];
  getY(D, 2, s);
  getY(L, 2, t); setY(D, 2, t);
  getY(U, 2, t); setY(L, 2, t);
  getY(R, 2, t); setY(U, 2, t);
                 setY(R, 2, s);
}
void rotate_S() {
  push(S);
  struct SQUARE s[3], t[3];
  getY(D, 1, s);
  getY(R, 1, t); setY(D, 1, t);
  getY(U, 1, t); setY(R, 1, t);
  getY(L, 1, t); setY(U, 1, t);
                 setY(L, 1, s);
}
void rotate_Sp() {
  push(S); push(S); push(S);
  struct SQUARE s[3], t[3];
  getY(D, 1, s);
  getY(L, 1, t); setY(D, 1, t);
  getY(U, 1, t); setY(L, 1, t);
  getY(R, 1, t); setY(U, 1, t);
                 setY(R, 1, s);
}
void rotate_B() {
  push(B);
  rotate(B, CCW);
  struct SQUARE s[3], t[3];
  getY(D, 0, s);
  getY(L, 0, t); setY(D, 0, t);
  getY(U, 0, t); setY(L, 0, t);
  getY(R, 0, t); setY(U, 0, t);
                 setY(R, 0, s);
}
void rotate_Bp() {
  push(B); push(B); push(B);
  rotate(B, CW);
  struct SQUARE s[3], t[3];
  getY(D, 0, s);
  getY(R, 0, t); setY(D, 0, t);
  getY(U, 0, t); setY(R, 0, t);
  getY(L, 0, t); setY(U, 0, t);
                 setY(L, 0, s);
}
void rotate_R() {
  push(R);
  rotate(R, CCW);
  struct SQUARE s[3], t[3];
  getX(D, 2, s);
  getX(B, 2, t); setX(D, 2, t);
  getX(U, 0, t); setXv(B, 2, t);
  getX(F, 2, t); setXv(U, 0, t);
                 setX(F, 2, s);
}
void rotate_Rp() {
  push(R); push(R); push(R);
  rotate(R, CCW);
  struct SQUARE s[3], t[3];
  getX(D, 2, s);
  getX(F, 2, t); setX(D, 2, t);
  getX(U, 0, t); setXv(F, 2, t);
  getX(B, 2, t); setXv(U, 0, t);
                 setX(B, 2, s);
}
void rotate_L() {
  push(L);
  rotate(L, CCW);
  struct SQUARE s[3], t[3];
  getX(D, 0, s);
  getX(F, 0, t); setX(D, 0, t);
  getX(U, 2, t); setXv(F, 0, t);
  getX(B, 0, t); setXv(U, 2, t);
                 setX(B, 0, s);
}
void rotate_Lp() {
  push(L); push(L); push(L);
  rotate(L, CW);
  struct SQUARE s[3], t[3];
  getX(D, 0, s);
  getX(B, 0, t); setX(D, 0, t);
  getX(U, 2, t); setXv(B, 0, t);
  getX(F, 0, t); setXv(U, 2, t);
                 setX(F, 0, s);
}
void rotate_M() {
  push(M);
  struct SQUARE s[3], t[3];
  getX(D, 1, s);
  getX(F, 1, t); setX(D, 1, t);
  getX(U, 1, t); setXv(F, 1, t);
  getX(B, 1, t); setXv(U, 1, t);
                 setX(B, 1, s);
}
void rotate_Mp() {
  push(M); push(M); push(M);
  struct SQUARE s[3], t[3];
  getX(D, 1, s);
  getX(B, 1, t); setX(D, 1, t);
  getX(U, 1, t); setXv(B, 1, t);
  getX(F, 1, t); setXv(U, 1, t);
                 setX(F, 1, s);
}
int main(int argc, char* argv[])
{
  int c=0;
  clrscr();
  init();
  nonblock(1);
  while (c != 'q') {
    usleep (10);
    if(kbhit()) {
      c = fgetc(stdin);
      switch (c) {
        case 'F': rotate_F(); break;
        case 'f': rotate_Fp(); break;
        case 'S': rotate_S(); break;
        case 's': rotate_Sp(); break;
        case 'B': rotate_B(); break;
        case 'b': rotate_Bp(); break;
        case 'U': rotate_U(); break;
        case 'u': rotate_Up(); break;
        case 'E': rotate_E(); break;
        case 'e': rotate_Ep(); break;
        case 'D': rotate_D(); break;
        case 'd': rotate_Dp(); break;
        case 'L': rotate_L(); break;
        case 'l': rotate_Lp(); break;
        case 'M': rotate_M(); break;
        case 'm': rotate_Mp(); break;
        case 'R': rotate_R(); break;
        case 'r': rotate_Rp(); break;
        case 'q':
        case 'Q': break;
        default: c = 0;
      }
      if (c != 'q' && c != 'Q' && c > 0) dump();
    }
  }
  nonblock(0);

  return 0;
}
