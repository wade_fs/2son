這個是『自學Python——編程基礎、科學計算及數據分析』繁體中文包

首先，系統中要安裝底下的套件:
python2: python + pip
python3: python3 + python3-pip

為了跟上時代，建議採用 python3
sudo pip3 install jupyter ipython numpy scipy pandas

第二、閱讀 .ipynb 的檔
  這種檔，你可以視為網頁，但是它的伺服器需要你自己啟動:

1) 切換到 .ipynb 的目錄
2) 執行 jupyter notebook
3) 正常的話，會叫出瀏覽器，或是自己瀏覽: localhost:8888
接下來就是用瀏覽器讀它了
4) 用瀏覽器瀏覽 index.ipynb
  http://localhost:8888/notebooks/index.ipynb
