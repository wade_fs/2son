#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int possible[5040];
int count=0;

int candidate[][24] = {
    { 1234, 1243, 1324, 1342, 1423, 1432, 2134, 2143, 2314, 2341, 2413, 2431, 3124, 3142, 3214, 3241, 3412, 3421, 4123, 4132, 4213, 4231, 4312, 4321 },
    { 5678, 5687, 5768, 5786, 5867, 5876, 6578, 6587, 6758, 6785, 6857, 6875, 7568, 7586, 7658, 7685, 7856, 7865, 8567, 8576, 8657, 8675, 8756, 8765 },
    { 2468, 2486, 2648, 2684, 2846, 2864, 4268, 4286, 4628, 4682, 4826, 4862, 6248, 6284, 6428, 6482, 6824, 6842, 8246, 8264, 8426, 8462, 8624, 8642 },
    { 1357, 1375, 1537, 1573, 1735, 1753, 3157, 3175, 3517, 3571, 3715, 3751, 5137, 5173, 5317, 5371, 5713, 5731, 7135, 7153, 7315, 7351, 7513, 7531 },
    {  237,  273,  327,  372,  723,  732, 2037, 2073, 2307, 2370, 2703, 2730, 3027, 3072, 3207, 3270, 3702, 3720, 7023, 7032, 7203, 7230, 7302, 7320 },
};

int getC(int n, int i) {
  switch (i) {
    case 1: return n/1000;
    case 2: return (n/100)%10;
    case 3: return (n/10)%10;
    case 4: return n%10;
    default: return -1;
  }
}

int matchAB(int n, int m) {
  int N1[] = { n/1000, (n/100)%10, (n/10)%10, n%10 };
  int N2[] = { m/1000, (m/100)%10, (m/10)%10, m%10 };
  int A=0, B=0;
  for (int i=0; i<4; i++) {
    for (int j=0; j<4; j++) {
      if (N1[i] == N2[j]) B++;
    }
  }
  for (int i=0; i<4; i++) {
    if (N1[i] == N2[i]) { A++; B--; }
  }
  return (A<<4)|B;
}

int getCadidate(int i) {
  if (i < 0 || i > 4) i = (rand()%4);
  int j = rand()%24;
  return candidate[i][j];
}

int initPossible() {
  int idx=0;
  for (int i=0; i<=9876; i++) {
    int a, b, c, d;
    a = (i/1000);
    b = (i/100)%10;
    c = (i/10)%10;
    d = i%10;
    if (a!=b && a!=c && a!=d && b!=c && b!=d && c!=d) {
      possible[idx++] = i;
    }
  }
  return idx;
}

int matchPossible(int n, int AB) {
  int min = 0;
  for (int i=0; i<count; i++) {
    if (matchAB(n, possible[i]) == AB) {
      if (i != min) {
        possible[min] = possible[i];
      }
      min++;
    }
  }
  //min = fMin;
  //max = fMax;
  return min;
}

int main(int argc, char* argv[]) {
  srand((unsigned int)time(NULL));
  count = initPossible();
  //for (int i=0; i<10; i++) printf ("%d\n", getCadidate(-1));
  int guest, A, B, AB;
  while (scanf ("%d %d %d", &guest, &A, &B) == 3) {
    AB = (A << 4) | B;
    printf ("match %d with %dA%dB, get count %d\n", guest, (AB&0xF0)>>4, AB&0xF, (count = matchPossible(guest, AB)));
  }
  return 0;
}
