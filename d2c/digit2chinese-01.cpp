#include <iostream>
#include <string>
using namespace std;

string ch[10] = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };

int main(int argc, char* argv[])
{
  string in;
  do {
    cout << "請輸入數字: ";
    cin >> in;
    for (int j=0; j<in.length(); j++) {
      cout << ch[in.at(j)-'0'];
    }
    cout << endl;
  } while (in.at(0) != '0');
  return 0;
}
