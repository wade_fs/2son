#include <iostream>
#include <string>
using namespace std;

string ch[10] = { "零", "壹", "貳", "參", "肆", "伍", "陸", "柒", "捌", "玖" };
string adv[10] = { "", "萬", "億", "兆", "京" };
string adv2[4] = { "", "拾", "百", "千" };
string stack[40];
int stackIdx=0;
int advIdx=0;

void push(string s) {
  stack[stackIdx++] = s;
}
string pop() {
  return stack[--stackIdx];
}
void trans(string s) {
  bool last0 = true;
  for (int i=0; i<s.length(); i++) {
    int j = s.length() - i - 1;
    if (last0) {
      if (s.at(j) != '0') {
        push(adv2[i]);
        push(ch[s.at(j)-'0']);
        last0 = false;
      }
    } else {
      if (s.at(j) == '0') {
        push (ch[0]);
        last0 = true;
      } else {
        push(adv2[i]);
        push(ch[s.at(j)-'0']);
      }
    }
  }
}
void dump() {
  while (stackIdx > 0) {
    cout << pop();
  }
  cout << endl;
}
int main(int argc, char* argv[])
{
  string in;
  cout << "請輸入數字: ";
  cin >> in;
  while (in.length() > 0) {
    push(adv[advIdx++]);
    if (in.length() >= 4) {
      trans(in.substr(in.length()-4, 4));
      in = in.substr(0, in.length() - 4);
    } else {
      trans(in);
      in = "";
    }
  }
  dump();
  return 0;
}
