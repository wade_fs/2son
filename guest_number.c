#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int possible[5040];
int count=0;
int candidate[][24] = {
    { 1234, 1243, 1324, 1342, 1423, 1432, 2134, 2143, 2314, 2341, 2413, 2431, 3124, 3142, 3214, 3241, 3412, 3421, 4123, 4132, 4213, 4231, 4312, 4321 },
    { 5678, 5687, 5768, 5786, 5867, 5876, 6578, 6587, 6758, 6785, 6857, 6875, 7568, 7586, 7658, 7685, 7856, 7865, 8567, 8576, 8657, 8675, 8756, 8765 },
    { 2468, 2486, 2648, 2684, 2846, 2864, 4268, 4286, 4628, 4682, 4826, 4862, 6248, 6284, 6428, 6482, 6824, 6842, 8246, 8264, 8426, 8462, 8624, 8642 },
    { 1357, 1375, 1537, 1573, 1735, 1753, 3157, 3175, 3517, 3571, 3715, 3751, 5137, 5173, 5317, 5371, 5713, 5731, 7135, 7153, 7315, 7351, 7513, 7531 },
    {  237,  273,  327,  372,  723,  732, 2037, 2073, 2307, 2370, 2703, 2730, 3027, 3072, 3207, 3270, 3702, 3720, 7023, 7032, 7203, 7230, 7302, 7320 },
    { 2569, 2596, 2659, 2695, 2956, 2965, 5269, 5296, 5629, 5692, 5926, 5962, 6259, 6295, 6529, 6592, 6925, 6952, 9256, 9265, 9526, 9562, 9625, 9652 },
    {  147,  174,  417,  471,  714,  741, 1047, 1074, 1407, 1470, 1704, 1740, 4017, 4071, 4107, 4170, 4701, 4710, 7014, 7041, 7104, 7140, 7401, 7410 },
    {  369,  396,  639,  693,  936,  963, 3069, 3096, 3609, 3690, 3906, 3960, 6039, 6093, 6309, 6390, 6903, 6930, 9036, 9063, 9306, 9360, 9603, 9630 },
    {  258,  285,  528,  582,  825,  852, 2058, 2085, 2508, 2580, 2805, 2850, 5028, 5082, 5208, 5280, 5802, 5820, 8025, 8052, 8205, 8250, 8502, 8520 },
};
int COL_CANDIDATE = sizeof(candidate)/sizeof(candidate[0]);

int getC(int n, int i) {
  switch (i) {
    case 1: return n/1000;
    case 2: return (n/100)%10;
    case 3: return (n/10)%10;
    case 4: return n%10;
    default: return -1;
  }
}

int matchAB(int n, int m) {
  int N1[] = { n/1000, (n/100)%10, (n/10)%10, n%10 };
  int N2[] = { m/1000, (m/100)%10, (m/10)%10, m%10 };
  int A=0, B=0;
  for (int i=0; i<4; i++) {
    for (int j=0; j<4; j++) {
      if (N1[i] == N2[j]) B++;
    }
  }
  for (int i=0; i<4; i++) {
    if (N1[i] == N2[i]) { A++; B--; }
  }
  return (A<<4)|B;
}

int getCadidate() {
  int i, r;
  do {
    i = rand()%COL_CANDIDATE;
  } while (candidate[i][0] == 0);
  r = candidate[i][rand()%24];
  for (int j=0; j<24; j++) candidate[i][j] = 0;
  return r;
}

int initPossible() {
  int idx=0;
  for (int i=0; i<=9876; i++) {
    int a, b, c, d;
    a = (i/1000);
    b = (i/100)%10;
    c = (i/10)%10;
    d = i%10;
    if (a!=b && a!=c && a!=d && b!=c && b!=d && c!=d) {
      possible[idx++] = i;
    }
  }
  return idx;
}

int matchPossible(int n, int AB) {
  int min = 0;
  for (int i=0; i<count; i++) {
    if (matchAB(n, possible[i]) == AB) {
      if (i != min) {
        possible[min] = possible[i];
      }
      min++;
    }
  }
  return min;
}

void win(int guest) {
}

int main(int argc, char* argv[]) {
  srand((unsigned int)time(NULL));
  count = initPossible();
  int guest, A, B, AB;
  for (int i=0; i<3; i++) {
    guest = getCadidate(-1);
    printf ("電腦猜 %04d, 請問是幾A幾B? \n", guest);
    scanf ("%d %d", &A, &B);
    AB = (A << 4) | B;
    count = matchPossible(guest, AB);
    if (count == 0) {
      printf ("不可能，你回答的幾A幾B一定錯誤!!!\n");
      break;
    }
    else if (count == 1) {
      printf ("哈哈，我猜到了，你出的題目是 %d\n", possible[0]);
      break;
    }
    else printf ("match %d with %dA%dB, get count %d\n", guest, (AB&0xF0)>>4, AB&0xF, count);
  }
  
  while (count > 1) {
    guest = possible[rand()%count];
    printf ("電腦猜 %04d, 請問是幾A幾B? \n", guest);
    scanf ("%d %d", &A, &B);
    AB = (A << 4) | B;
    count = matchPossible(guest, AB);
    if (count == 0) {
      printf ("不可能，你回答的幾A幾B一定錯誤!!!\n");
      break;
    }
    else if (count == 1) {
      printf ("哈哈，我猜到了，你出的題目是 %d\n", possible[0]);
      break;
    }
    else printf ("match %d with %dA%dB, get count %d\n", guest, (AB&0xF0)>>4, AB&0xF, count);

  }
  return 0;
}
