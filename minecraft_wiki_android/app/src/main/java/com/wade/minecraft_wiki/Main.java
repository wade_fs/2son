package com.wade.minecraft_wiki;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Main extends AppCompatActivity {
    private final static String TAG="MyLog";
    private final static String JavaBind ="Android";
    private Main context;

    WebView myBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        context = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        copyWWW();

        myBrowser = (WebView)findViewById(R.id.webview);
        setBrowser();
        myBrowser.loadUrl("file://"+Environment.getExternalStorageDirectory()+"/html/index.html");
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private boolean checkWWW() {
        File www = new File(Environment.getExternalStorageDirectory()+"/html/index.html");
        File minecraft_wik = new File(Environment.getExternalStorageDirectory()+"/html/index.html");
        if (www.exists() && minecraft_wik.exists()) return true;
        else return false;
    }
    private void copyWWW() {
        if (!checkWWW()) {
            warn("需要花點時間安裝資料檔，請耐心等候...", 2);
            try {
                InputStream _inputStream = getAssets().open("html.zip");
                String outpath = Environment.getExternalStorageDirectory().getPath();
                try  {
                    ZipInputStream zin = new ZipInputStream(_inputStream);
                    ZipEntry ze = null;
                    while ((ze = zin.getNextEntry()) != null) {
                        warn("解壓項目:"+ze.getName(), 1);
                        if(ze.isDirectory()) {
                            _dirChecker(outpath+"/"+ze.getName());
                        } else {
                            FileOutputStream fout = new FileOutputStream(outpath+"/" + ze.getName());
                            byte[] buf = new byte[1024];
                            for (int c = zin.read(buf); c != -1; c = zin.read(buf)) {
                                fout.write(buf, 0, c);
                            }

                            zin.closeEntry();
                            fout.close();
                        }
                    }
                    zin.close();
                    warn("恭喜！資料檔安裝完成", 0);
                } catch(Exception e) {
                    warn("複製檔案失敗:" + e.toString(), 1);
                }
            } catch (FileNotFoundException e) {
                warn("html.zip 不存在", 1);
            } catch (IOException e) {
                warn("無法找到 html.zip 資源檔: "+e.toString(), 1);
            }
        }
    }
    private void _dirChecker(String dir) {
        File f = new File(dir);
        f.mkdirs();
    }
    private void setBrowser() {
        myBrowser.addJavascriptInterface(new WebAppInterface(this), JavaBind);
        WebSettings mWebSettings = myBrowser.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setAllowFileAccessFromFileURLs(true);
        mWebSettings.setAllowUniversalAccessFromFileURLs(true);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setDefaultFontSize(14);
        myBrowser.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        myBrowser.setScrollbarFadingEnabled(false);
        myBrowser.setVerticalScrollBarEnabled(true);
    }

    // 0 : both, 1: log.d, 2: Toast
    public void warn(String msg, int mode) {
        if (mode != 2) Log.d("MyLog", msg);
        if (mode != 1) Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    // 底下是從 javascript 要往 java 呼叫函數時使用，
    // 在 javascript 中大約等於 Android.XXX(), where XXX 定義在底下
    //
    // 如果要從 APK 來呼叫 JavaScript, 則簡單的透過 myBrowser.loadUrl("javascript:YYY(....)");
    // 其中 YYY 定義在 javascript 中
    public class WebAppInterface {
        Context context;
        WebAppInterface(Context c) {
            context = c;
        }

        @JavascriptInterface
        public void showKeyboard() {
            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        @JavascriptInterface
        public void showToast(String toast) {
            warn(toast, 0);
        }
    }
}
