#include <sqlite3.h>
#include <stdlib.h>
#include <stdio.h>

#define CMD_CREATE_TABLE    "CREATE TABLE IF NOT EXISTS MyTable( column1 int, column2 char(20) );"
#define CMD_INSERT_ITEM        "INSERT INTO MyTable VALUES( 1 , 'Hello World' );"
#define CMD_QUERY        "SELECT * FROM MyTable;"

int Callback_ShowList( void *context, int count, char **values, char ** columnName )
{
    int i;
    context = NULL;
    for( i=0 ; i<count ; ++i )
        printf( "\t\t%s = %s\n" , columnName[i] , values[i] ? values[i] : "NULL" );
    printf( "\n" );
    return SQLITE_OK;
}

int main( int argc , char* argv[] )
{
    char *error_report = NULL;
    sqlite3 *db = NULL;

    if( argc < 2 )
    {
        fprintf( stderr , "Usage> %s sqlite_db_file\n" , argv[0] );
        exit(1);
    }

    printf( "1> Open a database\n" );
    if( sqlite3_open( argv[1] , &db ) != SQLITE_OK )
    {
        fprintf( stderr , "\t> Cannot open databases: %s\n" , sqlite3_errmsg(db) );
        sqlite3_close( db );
        exit(1);
    }
    else
        printf( "\t> Finish\n" );

    printf( "2> Create a table\n" );
    if( sqlite3_exec( db , CMD_CREATE_TABLE , Callback_ShowList , NULL , &error_report ) != SQLITE_OK )
    {
        fprintf( stderr , "\t> CMD: %s , Error: %s\n" , CMD_CREATE_TABLE , error_report );
        sqlite3_close(db);
        exit(1);
    }
    else
        printf( "\t> Finish\n" );

    printf( "3> Insert a data\n" );
    if( sqlite3_exec( db , CMD_INSERT_ITEM , Callback_ShowList , NULL , &error_report ) != SQLITE_OK )
    {
        fprintf( stderr , "\t> CMD: %s , Error: %s\n" , CMD_INSERT_ITEM , error_report );
        sqlite3_close(db);
        exit(1);
    }
    else
        printf( "\t> Finish\n" );

    printf( "4> Query\n" );
    if( sqlite3_exec( db , CMD_QUERY , Callback_ShowList , NULL , &error_report ) != SQLITE_OK )
    {
        fprintf( stderr , "\t> CMD: %s , Error: %s\n" , CMD_QUERY , error_report );
        sqlite3_close(db);
        exit(1);
    }
    else
        printf( "\t> Finish\n" );


    if( argc > 2 )
    {
        printf( "N> Do argv[2] ...\n" );
        if( sqlite3_exec( db , argv[2] , Callback_ShowList , NULL , &error_report ) != SQLITE_OK )
        {
            fprintf( stderr , "\t> CMD: %s, Error: %s\n" , argv[2] , error_report );
            sqlite3_close(db);
            exit(1);
        }
        else
            printf( "\t> Finish\n" );
    }
    return 0;
}
