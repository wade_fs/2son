#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
  int id;
  int h;
  int parent;    // 這個範例的特殊性，每個節點的 id 是照順序的，所以直接用整數 id 來指向父節點
  int nChildren;
  int *children; // 同上，子節點就是用整數陣列來存，正常的話 parent 應該是 Node*, children 是 Node**
} Node;

void setChildrenNumber(Node* nodes, int me, int nChildren) {
  nodes[me-1].children = (int*)calloc(nChildren, sizeof(int));
  nodes[me-1].nChildren = nChildren;
}

void setParent(Node* nodes, int parent, int child, int order) {
  nodes[child-1].parent = parent;
  nodes[parent-1].children[order] = child;

  int n = child-1; // 往上重設 h, 也就是父節點的 h 等於子節點的 h+1, 要往上找到 root node
  while (nodes[n].parent > 0) {
    if (nodes[nodes[n].parent-1].h < (nodes[n].h+1)) nodes[nodes[n].parent-1].h = (nodes[n].h+1);
    n = nodes[n].parent-1;
  }
}

void dumpTree(Node* nodes, int n) {
  for (int i=0; i<n; ++i) {
    printf ("nodes[%d] : parent = %d, nChildren = %d, h = %d\n", (i+1), nodes[i].parent, nodes[i].nChildren, nodes[i].h);
  }
}

// tree H 的值，等於所有節點的 h 的和
int treeHeight(Node *nodes, int n) {
  int h = 0;
  for (int i=0; i<n; ++i) h += nodes[i].h;
  return h;
}

void initNode(Node* nodes, int id) {
  nodes[id-1].id = id;
  nodes[id-1].h = 0; // 葉節點的高度 h == 0
  nodes[id-1].parent = 0; // 本題特殊，所有節點 id 是從 1 開始的
  nodes[id-1].nChildren = 0;
  nodes[id-1].children = NULL;
}

int main(int argc, char* argv[])
{
  int n;
  Node *nodes;
  int nn; // 每個節點的子節點個數

  scanf ("%d", &n);
  // 一開始就建立所有節點，後面再來建立樹
  nodes = (Node*)calloc(n, sizeof(Node));
  for (int i=0; i<n; ++i) { // 每個子節點都先初始化為葉節點
    initNode(nodes, i+1); // id 是 index+1
  }

  // 接下來，一行一行讀節點資訊
  for (int i=0; i<n; ++i) {
    // 每一行的第一個數字，是該節點(id=i+1)擁有的子節點個數
    scanf("%d", &nn);
    if (nn > 0) { // 葉節點(nn == 0) 已初始化無需多處理，非葉節點後面會有子節點 id
      setChildrenNumber(nodes, i+1, nn);
      // 開始一個一個讀進子節點的 id
      for (int j=0; j<nn; ++j) {
        int childId;
        scanf("%d", &childId);
        setParent(nodes, (i+1), childId, j);
      }
    }
  }

  dumpTree(nodes, n);

  // 接下來，找根節點，從任意一個節點往上找都行，所以我們從 nodes[0] 開始找
  int root = 0;
  while (nodes[root].parent > 0) {
    root = nodes[root].parent-1;
  }

  // 最後，印結果
  printf ("root id = %d, h = %d\n", nodes[root].id, treeHeight(nodes, n));

  // 因為前面用 calloc() 配置記憶體，這邊要記得釋放
  free(nodes);
  return 0;
}
