編譯: make test3

執行：
1) 一次執行一個資料檔，例如 data1.txt
   cat data1.txt | ./test3

2) 一次執行所有資料:
   for D in `seq 1 1 6`; do cat data$D.txt | ./test3 ; done
