#include <stdio.h>
#include "kbhit.h"

int main(void)
{
  int ch=0;

  nonblock(1);
  while ((ch = kbhit()) != 'q') {
    if (ch < 0) continue;
    printf ("你輸入 '%c'\n", ch);
  }
  nonblock(0);
  return 0;
}
