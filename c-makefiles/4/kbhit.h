#ifndef __KBHIT__
#define __KBHIT__

#define KB_UP 65
#define KB_DOWN 66
#define KB_RIGHT 67
#define KB_LEFT 68

int kbhit(void);
void nonblock(int state);
#endif
