#include <stdio.h>
#include <string.h>
#include "kbhit.h"
#include "ansi.h"

int main(void)
{
  int ch=0;
  char buf[255];
  int dir=0;

  // 初始化，請一定要做這三行
  nonblock(1);
  bzero(buf, 255); // 將 buf 清為 0
  clrscr();

  while ((ch = kbhit()) != 'q') {
    if (ch < 0) continue;
    buf[0] = ch;
    switch (ch) {
      case KB_UP:    fputs(ESC YELLOW "UP" RESET,    stdout); break;
      case KB_DOWN:  fputs(ESC YELLOW "DOWN" RESET,  stdout); break;
      case KB_LEFT:  fputs(ESC YELLOW "LEFT" RESET,  stdout); break;
      case KB_RIGHT: fputs(ESC YELLOW "RIGHT" RESET, stdout); break;
      default: printf ("%s%s", ansi(ch*5%256, buf), ansi_r(ch*5%256, buf));
    }
  }

  // 結束，還原螢幕控制, 請一定要做底下這一行
  nonblock(0);

  return 0;
}
