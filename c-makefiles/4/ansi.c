#include <string.h>
#include <stdio.h>
#include "ansi.h"
// 只有前景色
char* ansi(int color, char* msg) {
  char s[256];
  sprintf(s, "%s%d%s%s%s", ESC, color, ESC_END, msg, RESET);
  return strndup(s, strlen(s));
}
// 背景色跟前景色互補
char* ansi_r(int color, char* msg) {
  char s[256];
  int f, g;
  f = 15 - color%16;
  g = 15 - color/16;
  color = f * 16 + g;
  sprintf(s, "%s%d%s%s%d%s%s%s", ESC_BG, color, ESC_END, ESC, (255-color), ESC_END, msg, RESET);
  return strndup(s, strlen(s));
}
// 背景色跟前景色都需要指定
char* ansi_2(int color_fg, int color_bg, char* msg) {
  char s[256];
  int f, g;
  sprintf(s, "%s%d%s%s%d%s%s%s", ESC_BG, color_bg, ESC_END, ESC, color_fg, ESC_END, msg, RESET);
  return strndup(s, strlen(s));
}
char* ansi_char(int color, char c) {
  char s[256];
  sprintf(s, "%s%d%s%c%s", ESC, color, ESC_END, c, RESET);
  return strndup(s, strlen(s));
}
