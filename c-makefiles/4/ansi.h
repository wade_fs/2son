#ifndef __ANSI__
#define __ANSI__
#include <stdio.h>
#define ESC    "\x1b[38;5;"
#define ESC_END "m"
#define ESC_BG    "\x1b[48;5;"
#define RESET   "\x1b[0m"

// 顏色
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"

// 方向控制
#define ESC_UP     "\x1b[1A"
#define ESC_DOWN   "\x1b[1B"
#define ESC_RIGHT  "\x1b[1C"
#define ESC_LEFT   "\x1b[1D"

// 快速函數區
#define clrscr() fputs("\e[1;1H\e[2J", stdout)

// 正常函數區
char* ansi(int color, char* msg);
char* ansi_r(int color, char* msg);
char* ansi_2(int color_fg, int color_bg, char* msg);
char* ansi_char(int color, char c);
#endif
