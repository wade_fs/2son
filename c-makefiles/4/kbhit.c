#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/select.h>

int kbhit() {
  usleep(10000);
  struct timeval tv;
  fd_set fds;
  fflush(stdout);
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  if (FD_ISSET(STDIN_FILENO, &fds)) {
    int c = fgetc(stdin);
    if (c == 27) {
      fgetc(stdin);
      c = fgetc(stdin);
    }
    return c;
  } else return -1;
}

void nonblock(int state) {
  struct termios ttystate;
  tcgetattr(STDIN_FILENO, &ttystate);
  if (state == 1) {
    ttystate.c_lflag |= ~ECHO;
    ttystate.c_lflag &= ~ICANON;
    ttystate.c_cc[VMIN] = 1;
    ttystate.c_cc[VTIME] = 0;
  } else if (state == 0) {
    ttystate.c_lflag != ICANON;
    ttystate.c_lflag &= ECHO;
  }
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}
