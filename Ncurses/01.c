#include <curses.h> // required
/* 請參考
   http://tldp.org/HOWTO/NCURSES-Programming-HOWTO/
   http://edlinuxeditor.blogspot.tw/p/ncurses-library-tutorial.html
  編譯時要加 -lncurses
  gcc 01.c -o 01 -lncurses
  或者準備一個檔叫 Makefile, 內容如下面兩行
CC=gcc
LDLIBS = -lncurses
*/
int r,c, // current row and column (upper-left is (0,0))
  nrows, // number of rows in window
  ncols; // number of columns in window

void draw(char dc) {
  move(r,c); // curses call to move cursor to row r, column c
  delch(); insch(dc); // curses calls to replace character under cursor by dc
  refresh(); // curses call to update screen
  r++; // go to next row
  // check for need to shift right or wrap around
  if (r == nrows) {
    r = 0;
    c++;
    if (c == ncols) c = 0;
  }
}

int main() {
  char d;
  WINDOW *wnd;

  wnd = initscr(); // curses call to initialize window
  cbreak(); // curses call to set no waiting for Enter key
  noecho(); // curses call to set no echoing
  getmaxyx(wnd,nrows,ncols); // curses call to find size of window
  clear(); // curses call to clear screen, send cursor to position (0,0)
  refresh(); // curses call to implement all changes since last refresh

  r = 0; c = 0;
  while (1) {
    d = getch(); // curses call to input from keyboard
    if (d == 'q') break; // quit?
    draw(d); // draw the character
  }

  endwin(); // curses call to restore the original window and leave
  return 0;
}
