#include <stdio.h>
#include <unistd.h>

/* 關於螢幕的控制:
  顏色，請見
  https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
  如果要控制字元:
  https://en.wikipedia.org/wiki/Control_character
  本程式主要來自 Python 教學:
  http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
*/
#define ESC_BG     "\x1b[48;5;"
#define ESC        "\x1b[38;5;"
// 底下 1000A 是指要向上1000個字, 同理 1000D 是指向左1000個字
#define ESC_UP     "\x1b[1000A"
#define ESC_DOWN   "\x1b[1000B"
#define ESC_RIGHT  "\x1b[1000C"
#define ESC_LEFT   "\x1b[1000D"
#define ESC_END    "m"
#define RESET      "\x1b[0m"

int main (int argc, char const *argv[])
{
  for (int i=0; i<=100; i++) {
      usleep(200000);
      printf("%s%d%%", ESC_LEFT, i);
      fflush(stdout);
  }
  printf ("\n");

  return 0;
}
