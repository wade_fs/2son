#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

/* 關於螢幕的控制:
  顏色，請見
  https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
  如果要控制字元:
  https://en.wikipedia.org/wiki/Control_character
  本程式主要來自 Python 教學:
  http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
*/
#define ESC_BG     "\x1b[48;5;"
#define ESC        "\x1b[38;5;"
// 底下 1000A 是指要向上1000個字, 同理 1000D 是指向左1000個字
#define ESC_UP     "\x1b[1000A"
#define ESC_DOWN   "\x1b[1000B"
#define ESC_RIGHT  "\x1b[1000C"
#define ESC_LEFT   "\x1b[1000D"
#define ESC_END    "m"
#define RESET      "\x1b[0m"

#define cls() printf("\e[1;1H\e[2J");

void sort(int xy[10][2], int n) {
  for (int j=0; j<n-1; j++) {
    for (int i=j+1; i<n; i++) {
      if (xy[i][1] < xy[j][1]) {
        int tmpX = xy[i][0];
        int tmpY = xy[i][1];
        xy[i][0] = xy[j][0];
        xy[i][1] = xy[j][1];
        xy[j][0] = tmpX;
        xy[j][1] = tmpY;
      }
    }
  }
}

int main (int argc, char const *argv[])
{
  char printable[] = {
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',
    'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '<', '>',
    '@', '#', '$', '%', '^', '&', '*', '(', ')', '+',
    'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'N', 'P',
    'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '=',
    'Z', 'X', 'C', 'V', 'B', 'M'};
  char puzzleLeft[40][40];
  char puzzleRight[40][40];
  int level, height, width; // 寬度 = 10+level*2;, 高度 = 3+level * 2;
  int xy[10][2];

  cls();
  srand((unsigned int)time(NULL));

  printf ("請輸入等級 1-10: ");
  scanf ("%d", &level);
  if (level > 10) level = 10;
  else if (level <= 0) level = 1;
  height = 3 + level * 2;
  width = 10 + level * 2;

  // 先把 puzzleLeft[][] 及 puzzleRight[][] 清空
  memset(puzzleLeft, 0, 40*25);
  memset(puzzleRight, 0, 40*25);

  // 先產生亂數位置座標
  for (int i=0; i<level; i++) {
    int same = 0;
    // 底下防止新的亂數點與前面的重覆....如果重覆就重新取一點
    do {
      xy[i][0] = rand()%width;
      xy[i][1] = rand()%height;
      for (int j=0; j<i; j++) {
        if ((xy[j][0] == xy[i][0]) && (xy[j][1] == xy[i][1])) { same = 1; break; }
      }
    } while (same);
  }
  sort(xy, level);

  int l=0, cl, cr;
  for (int j=0; j<height; j++) {
    for (int i=0; i<width; i++) {
      cl = printable[rand()%(sizeof(printable))];
      // 目前 (j, i) 等於選出來的亂數點位置
      if ((l < level) && (j == xy[l][1]) && (i == xy[l][0])) {
        // 底下保證左右兩點的字元不一樣
        do {
          cr = printable[rand()%(sizeof(printable))];
        } while (cr == cl);
        ++l;
      } else cr = cl;
      puzzleLeft[j][i] = cl;
      puzzleRight[j][i] = cr;
    }
  }
  for (int j=0; j<height; j++) {
      printf ("%s : %s\n", puzzleLeft[j], puzzleRight[j]);
  }
      //printf("%s%d%%", ESC_LEFT, i);
      //fflush(stdout);

  printf ("\n");
  return 0;
}
