#include <stdio.h>

/* 關於螢幕的控制:
  顏色，請見
  https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
  如果要控制字元:
  https://en.wikipedia.org/wiki/Control_character
*/
#define ESC    "\x1b[38;5;"
#define ESC_END "m"
#define RESET   "\x1b[0m"

int main (int argc, char const *argv[])
{
  for (int i=0; i<16; i++) {
    for (int j=0; j<16; j++) {
      int code = i*16+j;
      printf("%s%d%s%4d%s", ESC, code, ESC_END, code, RESET);
    }
    printf ("\n");
  }

  return 0;
}
