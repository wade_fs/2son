#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* 關於螢幕的控制:
  顏色，請見
  https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
  如果要控制字元:
  https://en.wikipedia.org/wiki/Control_character
  本程式主要來自 Python 教學:
  http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
*/
#define ESC_BG     "\x1b[48;5;"
#define ESC        "\x1b[38;5;"
// 底下 1000A 是指要向上1000個字, 同理 1000D 是指向左1000個字
#define ESC_UP     "\x1b[1000A"
#define ESC_DOWN   "\x1b[1000B"
#define ESC_RIGHT  "\x1b[1000C"
#define ESC_LEFT   "\x1b[1000D"
#define ESC_END    "m"
#define RESET      "\x1b[0m"

char bar[200];
void fill_bar(int p) {
}

void progress(int p, int w) {
  p = (p+1)*w/100; // 計算進度 p 占幾格
  // 將進度條填入 bar[] 中
  memset (bar, 0, 200);
  bar[0] = '[';
  bar[1] = '#';
  for (int i=0; i<p; i++) bar[i+2] = '#';
  for (int i=p; i<w; i++) bar[i+2] = ' ';
  bar[w+2] = ']';
  printf("%s%s", ESC_LEFT, bar);
}

int main (int argc, char const *argv[])
{
  for (int i=0; i<=100; i++) {
    usleep(200000); // 暫停 0.2 秒
    progress(i, 60); // 進度 i, 總寬度 60 格
    printf (" %d%%", i);
    fflush(stdout);
  }
  printf ("\n");

  return 0;
}
