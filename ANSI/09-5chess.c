#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* 關於螢幕的控制:
  顏色，請見 https://en.wikipedia.org/wiki/ANSI_escape_code
  比較正式的文件在 http://www.termsys.demon.co.uk/vtansi.htm
  如果要控制字元: https://en.wikipedia.org/wiki/Control_character
  本程式主要來自 Python 教學:
  http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
  跳脫字元有幾個寫法：
  \e == \033 == \x1b
  螢幕座標是從 (1,1) 開始，而不是 (0,0)
*/

#define RESET		"\x1b[0m"
// 底下是前景文字
#define COLOR_BLACK		"\x1b[30m"
#define COLOR_RED		"\x1b[31m"
#define COLOR_GREEN		"\x1b[32m"
#define COLOR_YELLOW		"\x1b[33m"
#define COLOR_BLUE		"\x1b[34m"
#define COLOR_MAGENTA		"\x1b[35m"
#define COLOR_CYAN		"\x1b[36m"
#define COLOR_WHITE		"\x1b[37m"
// 底下是亮色
#define COLOR_BBLACK		"\x1b[30;1m"
#define COLOR_BRED		"\x1b[31;1m"
#define COLOR_BGREEN		"\x1b[32;1m"
#define COLOR_BYELLOW		"\x1b[33;1m"
#define COLOR_BBLUE		"\x1b[34;1m"
#define COLOR_BMAGENTA		"\x1b[35;1m"
#define COLOR_BCYAN		"\x1b[36;1m"
#define COLOR_BWHITE		"\x1b[37;1m"
// 底下是背景色
#define COLOR_BG_BLACK		"\x1b[40m"
#define COLOR_BG_RED		"\x1b[41m"
#define COLOR_BG_GREEN		"\x1b[42m"
#define COLOR_BG_YELLOW		"\x1b[43m"
#define COLOR_BG_BLUE		"\x1b[44m"
#define COLOR_BG_MAGENTA	"\x1b[45m"
#define COLOR_BG_CYAN		"\x1b[46m"
#define COLOR_BG_WHITE		"\x1b[47m"
// 底下是亮背景色
#define COLOR_BG_BBLACK		"\x1b[40;1m"
#define COLOR_BG_BRED		"\x1b[41;1m"
#define COLOR_BG_BGREEN		"\x1b[42;1m"
#define COLOR_BG_BYELLOW	"\x1b[43;1m"
#define COLOR_BG_BBLUE		"\x1b[44;1m"
#define COLOR_BG_BMAGENTA	"\x1b[45;1m"
#define COLOR_BG_BCYAN		"\x1b[46;1m"
#define COLOR_BG_BWHITE		"\x1b[47;1m"
// 雜項
#define TEXT_RESET		"\x1b[0m"
#define TEXT_BOLD		"\x1b[1m"
#define TEXT_UNDERLINE		"\x1b[4m"
#define TEXT_BLINK		"\x1b[5m"
#define TEXT_REVERSED		"\x1b[7m"
#define TEXT_HIDDEN		"\x1b[8m"

#define clrscr() printf("\e[1;1H\e[2J")
#define clrscr2() printf("\e[2J")
#define printMsg(x,y,msg) printf("\x1b[%d;%dH%s",(y),(x),(msg))
#define gotoXY(x,y) printf("\x1b[%d;%dH",(y),(x))
#define gotoUp(n) printf("\x1b[%dA",(n))
#define gotoDown(n) printf("\x1b[%dB",(n))
#define gotoRight(n) printf("\x1b[%dC",(n))
#define gotoLeft(n) printf("\x1b[%dD",(n))
#define gotoLineBegin() printf("\x1b[200D")
#define gotoHome() printf("\x1b[H")
#define eraseToBeginOfLine() printf("\x1b[1K")
#define eraseToEndOfLine() printf("\x1b[K")
#define eraseLine() printf("\x1b[2K")
#define eraseUp() printf("\x1b[1J")
#define eraseDown() printf("\x1b[J")
#define saveCursor() printf("\x1b[s")
#define restoreCursor() printf("\x1b[u")
#define scrollScreen() printf("\x1b[r")
#define scrollScreen2(m,n) printf("\x1b[%d;%dr", \
                        ((m)<(n)?(m):(n)), ((m)<(n)?(n):(m)))
#define scrollDown() printf("\x1bM")

int board[19][19];
void initBoard() {
  memset(board, \0, 19*19);
/*
  for (int y=0; y<19; y++) {
    for (int x=0; x<19; x++) {
      board[y][x] = 0;
    }
  }
*/
}
int main (int argc, char const *argv[])
{
  int quit = 0;
  char buf[256];
  clrscr();
  do {
    printf ("請輸入，接受命令: quit, list, reset, cls 或座標 X,Y: ");
    scanf ("%s", buf);
    if (strncmp(buf, "cls", 3) == 0) {
      clrscr();
    } else if (strncmp(buf, "quit", 4) == 0) quit = 1;
  } while (!quit);
  return 0;
}
