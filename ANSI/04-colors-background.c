#include <stdio.h>

/* 關於螢幕的控制:
  顏色，請見
  https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
  如果要控制字元:
  https://en.wikipedia.org/wiki/Control_character
  本程式，主要來自 Python 的教學:
  http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
*/
#define ESC_BG    "\x1b[48;5;"
#define ESC    "\x1b[38;5;"
#define ESC_END "m"
#define RESET   "\x1b[0m"

int main (int argc, char const *argv[])
{
  // 底下是印前景文字顏色
  for (int i=0; i<16; i++) {
    for (int j=0; j<16; j++) {
      int code = i*16+j;
      printf("%s%d%s%4d%s", ESC, code, ESC_END, code, RESET);
    }
    printf ("\n");
  }
  // 底下是印背景色
  for (int i=0; i<16; i++) {
    for (int j=0; j<16; j++) {
      int code = i*16+j;
      printf("%s%d%s%4d%s", ESC_BG, code, ESC_END, code, RESET);
    }
    printf ("\n");
  }
  // 底下是前景背景同時印，然後讓背景與前景色互補，這樣文字會比較容易看到
  for (int i=0; i<16; i++) {
    for (int j=0; j<16; j++) {
      int code = i*16+j;
      printf("%s%d%s%s%d%s%4d%s", ESC_BG, (255-code), ESC_END, ESC, code, ESC_END, code, RESET);
    }
    printf ("\n");
  }

  return 0;
}
